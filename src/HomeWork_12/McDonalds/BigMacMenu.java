package HomeWork_12.McDonalds;

/**
 * Created by user on 06.11.2016.
 */
public class BigMacMenu {

    private boolean isHamburgerReady;
    private boolean isFriesReady;
    private boolean isColaReady;
    private boolean isBigMacMenuReady;

    public boolean isHamburgerReady() {
        return isHamburgerReady;
    }

    public boolean isFriesReady() {
        return isFriesReady;
    }

    public boolean isColaReady() {
        return isColaReady;
    }

    public boolean isBigMacMenuReady() {
        return isBigMacMenuReady;
    }

    public void setHamburgerReady(boolean isHamburgerReady) {
        this.isHamburgerReady = isHamburgerReady;
    }

    public void setFriesReady(boolean isFriesReady) {
        this.isFriesReady = isFriesReady;
    }

    public void setColaReady(boolean colaReady) {
        this.isColaReady = colaReady;
        isBigMacMenuReady = true;
    }

    public void setBigMacMenuReady(boolean isBigMacMenuReady) {
        this.isBigMacMenuReady = isBigMacMenuReady;
    }
}
