package HomeWork_12.McDonalds;

/**
 * Created by user on 06.11.2016.
 */
public class McDonaldsKitchen {

    private static BigMacMenu bigMacMenu = new BigMacMenu();

    public static void createBigMacMenu() {
        Thread hamburger = new Thread(() -> {
            System.out.println("Hamburger cooking started.");
            synchronized (bigMacMenu) {
                try {
                    Thread.sleep(2000);
                    bigMacMenu.setHamburgerReady(true);
                    bigMacMenu.notifyAll();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("Hamburger ready!");
            }
        });
        Thread fries = new Thread(() -> {
            System.out.println("Fries cooking started.");
            synchronized (bigMacMenu) {
                try {
                    System.out.println("Waiting for hamburger...");
                    while (!bigMacMenu.isHamburgerReady()) {
                        bigMacMenu.wait();
                    }
                    Thread.sleep(2000);
                    bigMacMenu.setFriesReady(true);
                    bigMacMenu.notifyAll();
                } catch (InterruptedException e) {
                        e.printStackTrace();
                }
                System.out.println("Fries ready!");
            }


        });

        Thread cola = new Thread(() -> {
            System.out.println("Cola cooking started.");
            synchronized (bigMacMenu) {
                try {
                    System.out.println("Waiting for fries...");
                    while (!bigMacMenu.isFriesReady()) {
                        bigMacMenu.wait();
                    }
                    Thread.sleep(2000);
                    bigMacMenu.setColaReady(true);
                    bigMacMenu.notifyAll();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("Cola ready!");
            }


        });

        hamburger.start();
        fries.start();
        cola.start();


    }

    public static void main(String[] args) throws InterruptedException{
        createBigMacMenu();

        synchronized (bigMacMenu){
            while(!bigMacMenu.isBigMacMenuReady()){
                bigMacMenu.wait();
            }

        }
        System.out.println("Big Mac menu is ready!!!");
    }
}
