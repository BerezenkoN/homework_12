package HomeWork_12.McDonalds;

/**
 * Created by user on 06.11.2016.
 */
public class McDonaldsJoin {

    private  static BigMacMenu bigMacMenu = new BigMacMenu();
    private static Thread hamburger, fries, cola;



        public static void createBigMacMenu() {
            hamburger = new Thread(() -> {
                System.out.println("Hamburger cooking started.");
                 try {
                     Thread.sleep(2000);
                 }
                    catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                bigMacMenu.setHamburgerReady(true);
                    System.out.println("Hamburger ready!");
            });
            fries = new Thread(() -> {
                System.out.println("Fries cooking started.");
                try {
                    System.out.println("Waiting for hamburger...");
                    hamburger.join();
                    Thread.sleep(2000);
                     } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                bigMacMenu.setFriesReady(true);
                    System.out.println("Fries ready!");
                });

             cola = new Thread(() -> {
                System.out.println("Cola cooking started.");
                 try {
                        System.out.println("Waiting for fries...");
                        fries.join();
                        Thread.sleep(2000);
                 } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                 bigMacMenu.setColaReady(true);
                    System.out.println("Cola ready!");
             });
            hamburger.start();
            fries.start();
            cola.start();
        }

        public static void main(String[] args) throws InterruptedException{
            createBigMacMenu();
            cola.join();

            System.out.println("Big Mac menu is ready!!!");
        }
    }


