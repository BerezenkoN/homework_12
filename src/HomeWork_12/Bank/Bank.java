package HomeWork_12.Bank;

/**
 * Created by user on 06.11.2016.
 */
public class Bank {
    static Account account = new Account(1000L);
    public static volatile boolean isBankWorking = true;

    public static void main(String[] args) throws InterruptedException {
        new AddMoney();
        new WithdrawMoney();
        new WithdrawMoney();
        new WithdrawMoney();
        new WithdrawMoney();
        isBankWorking = true;
        try {
            Thread.sleep(3000);
        }
        catch (InterruptedException e){
            e.printStackTrace();
        }
        isBankWorking = false;

    }

    static class Account {
        public volatile Long ballance = 0L;

        public Account(Long ballance) {
            this.ballance = ballance;
        }

        public synchronized void add(Long money) {
            ballance += money;
            System.out.println("Add '+' " + money + "; ballance: " + ballance);
        }

        public synchronized void withdraw(Long money) {
            ballance -= money;
            System.out.println("Withdraw '-' " + money + "; ballance: " + ballance);
        }
    }

    public static class AddMoney extends Thread {
        public AddMoney() {
            start();
        }

        public void run() {
            while (isBankWorking) {
                account.add(1000L);
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                    break;
                }

            }
        }
    }

    public static class WithdrawMoney extends Thread {
        public WithdrawMoney() {
            start();
        }

        public void run() {
            while (isBankWorking) {
                account.withdraw(500L);
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                    break;
                }

            }

        }
    }
}
