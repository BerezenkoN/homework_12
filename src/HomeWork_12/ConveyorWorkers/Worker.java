package HomeWork_12.ConveyorWorkers;

/**
 * Created by user on 04.11.2016.
 */
public class Worker extends Thread {
    public String name;

    public Worker(String nameOfWorker, String threadName) {
        super(threadName);
        this.name = nameOfWorker;
    }
    public void run() {
        System.out.println("Worker " + this.name + " started to work!");
        for(int i = 0; i < 1000; i++){
            System.out.println("Detail number " + i + " is done by" + this.name);
        }
    }
}

