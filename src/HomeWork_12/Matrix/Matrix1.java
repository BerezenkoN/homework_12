/*
package HomeWork_12.Matrix;

import java.io.BufferedWriter;

import java.io.FileWriter;

import java.io.IOException;

import java.util.Random;
*/
/**
 * Created by user on 06.11.2016.
 *//*

public class Matrix1 {

        static int firstMatrix[][] = new int[200][200];

        static int secondMatrix[][] = new int[200][200];

        static int resultMatrixSize = firstMatrix.length * secondMatrix[0].length;

        static int resultMatrix[][] = new int[firstMatrix.length][secondMatrix[0].length];



        public static void main(String[] args) throws InterruptedException, IOException {

            fillRandom(firstMatrix);

            fillRandom(secondMatrix);



            // Последовательное выполнение
            long start = System.currentTimeMillis();

            sequentialCalculation(firstMatrix, secondMatrix, resultMatrix, 0, resultMatrixSize);

            long time = System.currentTimeMillis() - start;

            System.out.println("Sequential calculation took: " + time + " milliseconds.");

            writeToFile(resultMatrix, "1.txt");



            // Параллельное выполнение на двух потоках

            start = System.currentTimeMillis();

            parallelCalculation(firstMatrix, secondMatrix, resultMatrix, 2);

            time = System.currentTimeMillis() - start;

            System.out.println("Parallel calculation (2) took: " + time + " milliseconds.");

            writeToFile(resultMatrix, "2.txt");



            // Параллельное выполнение на 4 потоках

            start = System.currentTimeMillis();

            parallelCalculation(firstMatrix, secondMatrix, resultMatrix, 4);

            time = System.currentTimeMillis() - start;

            System.out.println("Parallel calculation (4) took: " + time + " milliseconds.");

            writeToFile(resultMatrix, "4.txt");



            // Параллельное выполнение на 16 потоках

            start = System.currentTimeMillis();

            parallelCalculation(firstMatrix, secondMatrix, resultMatrix, 16);

            time = System.currentTimeMillis() - start;

            System.out.println("Parallel calculation (16) took: " + time + " milliseconds.");

            writeToFile(resultMatrix, "16.txt");

        }



        public static void fillRandom(int[][] matrix) {

            Random rnd = new Random();

            for (int i = 0; i < matrix.length; i++)

                for (int j = 0; j < matrix[0].length; j++) {

                    matrix[i][j] = rnd.nextInt(9);

                }

        }



        private static void parallelCalculation(int[][] firstMatrix,

                                                int[][] secondMatrix,

                                                int[][] resultMatrix,

                                                int threadCount) throws InterruptedException {



            //число элементов, которые будут расчитаны каждым потоком

            final int partSize = resultMatrixSize / threadCount;



            Thread[] threads = new Thread[threadCount];



            //каждый поток будет расчитывать только часть элементов результирующей матрицы

            for (int i = 0; i < threads.length; i++) {

                final int finalI = i;



                threads[i] = new Thread(() -> {

                    int from = finalI * partSize;
                    sequentialCalculation(firstMatrix, secondMatrix, resultMatrix, from, from + partSize);

                });

                threads[i].start();

            }



            for (Thread t : threads)

                t.join();//дожидаемся окончания работы потока



        }



        private static void sequentialCalculation(int[][] firstMatrix,

                                                  int[][] secondMatrix,

                                                  int[][] resultMatrix,

                                                  int from, int to) {

            for (int i = from; i < to; i++) {

                int row = i / firstMatrix.length;//считаем строку очередного элемента

                int col = i % secondMatrix[0].length;//считаем столбец очередного элемента

                //представляем матрицу, как список элементов MultiplyMatrixElement

                resultMatrix[row][col] = calculateMultipleValue(getRow(row, firstMatrix), getCol(col, secondMatrix));

            }

        }



        //вытаскивает всю строку по номеру

        private static int[] getRow(int rowNumber, int[][] matrix) {

            int[] row = new int[matrix[0].length];

            System.arraycopy(matrix[rowNumber], 0, row, 0, matrix[0].length);

            return row;

        }



        //вытаскивает весь столбец по номеру

        private static int[] getCol(int colNumber, int[][] matrix) {

            int[] col = new int[matrix.length];

            for (int i = 0; i < matrix.length; i++)

                col[i] = matrix[i][colNumber];

            return col;

        }



        private static void writeToFile(int[][] matrix, String fileName) throws IOException {

            BufferedWriter fileWriter = new BufferedWriter(new FileWriter(fileName));

            for (int i = 0; i < matrix.length; i++) {

                for (int j = 0; j < matrix[0].length; j++) {

                    fileWriter.write(matrix[i][j] + " ");

                }

                fileWriter.newLine();

            }

            fileWriter.close();

        }



        private static int calculateMultipleValue(int[] row, int[] col) {

            int result = 0;

            //перемножаем соответствующие элементы и складываем

            for (int i = 0; i < row.length; i++)

                result += row[i] * col[i];

            return result;

        }



    }



*/
